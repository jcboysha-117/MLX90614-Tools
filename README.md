MLX90614 Tools 

These tools are designed to allow for utilization of multiple MLX90614 IR 
Temperature Sensors. 

The Included tools allow readdressing of the sensors and a script that will 
allow an Arduino to gather all the necessary data from each chip individually. 
This allows for an array of sensors to be used to gather temperatures from 
different target objects and locations simultaneously in order to view a 
potential relationship.

To use these tools please follow the steps in the included HowToUse.txt file. 
Additional information can be found in the CONTENTS file and included 
subdirectories.

Additionally, with the advent of Arduino integration with Visual Studio and 
Windows 10 I will be creating an application that will allow for customization 
of addresses and greater ease of use. 