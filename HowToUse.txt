MLX90614 Tools for Radford Arctic Research. 
by J.C. Boysha
25-5-2015

Sections: 

	1) READ THESE NOTES FIRST
	2) HOW TO USE THE Readdress_MLX90614.ino SCRIPT
	3) HOW TO USE THE Three_IR_Read.ino SCRIPT
	4) SENSOR ADDRESSES


**---READ THESE NOTES FIRST---**

1) Before any work begins ensure that the electronics are set up as in the 
included diagram. It is very important that the circuitry be set up correctly.

2) When readdressing the MLX90614 it is very important that the circuit 
have only 1 MLX90614 attached. Having more than one attached can render the
MLX90614 unusable. 

3) Follow the steps in this readme. If there are any troubles with this code
or the attached circuit diagram email jboysha@radford.edu.

4) Before proceeding copy the i2cmaster folder from the Arduino_Scripts directory
to the Arduino libraries directory.

**--HOW TO USE THE Readdress_MLX90614.ino SCRIPT--**

Note: This script automatically has all addresses commented out. It will fail
to run at all if these steps are not followed. 

In order to utilize the script to read 3 MLX90614 sensors perform these steps

1) Construct the circuit as explained in the attached circuitry file. 
   (ReaddressCircuitry.pdf).

2) In the Readdress_MLX90614.ino file uncomment line 4. 
   (It should read:
    byte NewMLXAddr = 0x1A; //MLX 1
   )

3) Upload the ino file to the arduino and start a serial monitor. 

4) Insert the MLX90614 into the circuit and press the reset button on the
   arduino Uno. 

5) Verify the readdress has occurred by viewing the Serial data.

6) Remove the MLX90614 sensor

7) In the Readdress_MLX90614.ino file comment line 4. 

8) In the same file uncomment line 5
   (It should read:
    byte NewMLXAddr = 0x2A; //MLX 2
   )

9) Upload this file to the Arduino Uno and open a serial monitor.

10) Insert the second MLX90614 Sensor into the circuit and press the reset
    button on the arduino.

11) Verify the readdress has occurred by viewing the serial data. 

12) Remove the second MLX90614 Sensor from the circuit.

13) In the Readdress_MLX90614.ino file comment line 5. 

14) In the same file uncomment line 6.
    (It should read:
     byte NewMLXAddr = 0x3A; //MLX 3
    )

15) Upload the file to the Arduino Uno. 

16) Insert the final MLX90614 Sensor into the circuit.

17) Press the reset button on the Arduino Uno. 

18) Verify the readdress has occurred by viewing the serial data. 

At this point all of the MLX90614 sensors for the Three_IR_Read circuit should
have the appropriate addresses. 

**---HOW TO USE THE Three_IR_Read.ino SCRIPT---**

1) VERIFY THAT THE ARDUINO DOES NOT HAVE THE Readdress_MLX90614.ino SCRIPT ON
   IT CURRENTLY. FAILURE TO ENSURE THIS MAY RENDER ALL THREE SENSORS IN THE 
   DESCRIBED CIRCUIT UNUSABLE. 

2) Construct the circuit as explained in the attached circuitry file.
   (ReadCircuit.pdf)

3) Upload the Three_IR_Read.ino file to the Arduino Uno. 

4) Start a Serial Monitor and verify that data from each sensor is within the 
   norms for the given experiment. 

This should yield all necessary data. From here, any modifications to the
scripts or described circuits should be made carefully to ensure proper 
operation of the MLX90614 Sensors. 

**---SENSOR ADDRESSES---**

The MLX90614 Sensors modified using these instructions will have the following
addresses. (Assuming no modifications have been made to any of the attached
scripts).

Sensor 1	0x1A
Sensor 2	0x2A
Sensor 3	0x3A

The default sensor address (If the sensors ever need to be set back to factory
default for any reason) is 0x5A. Setting the sensor to this address can be 
accomplished by following the following steps. 

1) Construct the circuit as described in the circuitry file.
   (ReaddressCircuit.pdf)

2) In the Readdress_MLX90614.ino File uncomment line 7.
   (It should read:
    byte NewMLXAddr = 0x5A; //DEFAULT MLX ADDRESS
   )

3) Upload the Readdress_MLX90614.ino file to the Arduino Uno. 

4) Insert the MLX90614 Sensor to be set to default address. 

5) Press the reset button on the Arduino Uno.

6) Verify the readdress has occurred by viewing the serial data. 
