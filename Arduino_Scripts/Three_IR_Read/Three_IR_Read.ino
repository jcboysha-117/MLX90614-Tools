/**
 * Three Infrared Thermometers MLX90614
 * Written and commented by J.C. Boysha for the Radford Arctic Research Team
 * Utilizing code from:
 *
 * Two Infrared Thermometers MLX90614
 * by Jaime Patarroyo (JP)
 * based on 'Is it hot? Arduino + MLX90614 IR Thermometer' by bildr.blog
 * 
 */

#include <i2cmaster.h>

int DELAY = 1000; //Change this variable to change capture time of all three 
                  //temperatures. 

/*
 *
 ****---CHANGE NO CODE BELOW THIS POINT---****
 *
 */

int device1Address = 0x1A<<1; // MLX 1 Address   
int device2Address = 0x2A<<1; // MLX 2 Address
int device3Address = 0x3A<<1; // MLX 3 Address

//Code from JP
float celcius1 = 0;             // Variable to hold temperature in Celcius
                                // for sensor 1.
float celcius2 = 0;             // Variable to hold temperature in Celcius
                                // for sensor 2.
//End JP Code

float celcius3 = 0;             // Variable for Celsius temp from Sensor 3

void setup()
{
  //Stock MLX Code
  Serial.begin(9600);           // Start serial communication at 9600bps.
  
  i2c_init();                               // Initialise the i2c bus.
  PORTC = (1 << PORTC4) | (1 << PORTC5);    // Enable pullups.
  //End Stock Code
}

void loop()
{
  celcius1 = temperatureCelcius(device1Address);//Read Celsius Temperature from Sensor 1
  celcius2 = temperatureCelcius(device2Address);//Read Celsius Temperature from Sensor 2
  celcius3 = temperatureCelcius(device3Address);//Read Celsius Temperature from Sensor 3

// JP Code
  Serial.print("Sensor 1: Celcius: ");   // Prints all readings to the Serial 
  Serial.println(celcius1);              // port.
  Serial.print("Sensor 2: Celcius: ");
  Serial.println(celcius2);
// End JP Code
  Serial.print("Sensor 3: Celcius: ");
  Serial.println(celcius3);

  delay(DELAY);                         // Wait the delay time before printing again.
}

//Code from JP

float temperatureCelcius(int address) {
  int dev = address;
  int data_low = 0;
  int data_high = 0;
  int pec = 0;

  // Write
  i2c_start_wait(dev+I2C_WRITE);
  i2c_write(0x07);

  // Read
  i2c_rep_start(dev+I2C_READ);
  data_low = i2c_readAck();       // Read 1 byte and then send ack.
  data_high = i2c_readAck();      // Read 1 byte and then send ack.
  pec = i2c_readNak();
  i2c_stop();

  // This converts high and low bytes together and processes temperature, 
  // MSB is a error bit and is ignored for temps.
  double tempFactor = 0.02;       // 0.02 degrees per LSB (measurement 
                                  // resolution of the MLX90614).
  double tempData = 0x0000;       // Zero out the data
  int frac;                       // Data past the decimal point

  // This masks off the error bit of the high byte, then moves it left 
  // 8 bits and adds the low byte.
  tempData = (double)(((data_high & 0x007F) << 8) + data_low);
  tempData = (tempData * tempFactor)-0.01;
  float celcius = tempData - 273.15;
  
  // Returns temperature un Celcius.
  return celcius;
}
//End Code from JP
